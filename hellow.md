<!-- Encabezados -->

# my title
## my title h2
### my title h3
#### my title h4
##### my title h5
###### my title h6

this is an *italic* text

this is an **strong** text

<!--UL -->
* apple
    * apple 2
* orange
* etc
  
1. apple
   1. apple 2
2. 2.organge
3. etc

[Netflix.com](http://www.netflix.com)

[Netflix.com](http://www.netflix.com "Titulo")

>this is a quote

---
___
